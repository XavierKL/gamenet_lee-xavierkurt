using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceManager : MonoBehaviour
{

    public GameObject[] carsWithGuns;
    public Transform[] startingPositions;
    public GameObject[] eliminationTextUI;

    public Text timeText;

    public static DeathRaceManager instance = null;
    
    void Awake(){

        if (instance == null){
            
            instance = this;
        }

        else if (instance != null){

            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady){

            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)){

                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(carsWithGuns[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }

        foreach (GameObject go in eliminationTextUI){

            go.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
