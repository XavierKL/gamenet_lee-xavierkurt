using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public enum GunType{

    hitScan,
    projectile
};

public class GunScripts : MonoBehaviourPunCallbacks
{

    public GunType gunType;
    public GameObject bulletPrefab;
    public Transform bulletPoint;
    public float fireRate;
    private float firerateTimer = 0;
    private float bulletSpeed;
    bool canShoot;

    [SerializeField]
    Camera fpsCamera;

    [Header ("Health related objects")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    
    // Start is called before the first frame update
    void Start()
    {

        switch (gunType){

            case GunType.hitScan:
            fireRate = .2f;
            break;

            case GunType.projectile:
            bulletPrefab = GameObject.FindGameObjectWithTag("Projectile");
            //bulletPoint = GameObject.Find("Bullet Point").transform;
            bulletSpeed = 200f;
            canShoot = true;
            break;

            default:
            break;
        }

        health = startHealth;
        healthBar.fillAmount = health / 100;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (firerateTimer < fireRate){

            firerateTimer += Time.deltaTime;
        }

        Shoot();
    }

    IEnumerator ProjectileCooldown(){

        canShoot = false;
        yield return new WaitForSeconds(2f);
        canShoot = true;
    }

    public void Shoot(){

        switch (gunType){

            case GunType.hitScan:

            if (Input.GetButton("Fire1") && firerateTimer > fireRate){

                firerateTimer = 0.0f;
                Ray ray = fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100)){

                    Debug.Log(hit.collider.gameObject.name);

                    if (hit.collider.gameObject.CompareTag("Car") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine){

                        hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
                    }
                }
            }

            break;

            case GunType.projectile:
            
            if (Input.GetButton("Fire1") && canShoot == true){

                bulletPrefab = PhotonNetwork.Instantiate("Projectile", bulletPoint.position, Quaternion.identity);
                Rigidbody body = bulletPrefab.GetComponent<Rigidbody>();
                body.AddForce(bulletPoint.forward * bulletSpeed, ForceMode.Impulse);
                StartCoroutine(ProjectileCooldown());
            }
            break;

            default:
            break;
        }
    }
    

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info){

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0){

            GetComponent<EliminationController>().Eliminated();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }
    
}
