using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerListItemInitializer : MonoBehaviour
{
    
    [Header("UI References")]
    public Text PlayerNameText;
    public Button PlayerReadyButton;
    public Image playerReadyImage;

    private bool isPlayerReady = false;

    public void Initialize(int playerID, string playerName){

        PlayerNameText.text = playerName;

        if (PhotonNetwork.LocalPlayer.ActorNumber != playerID){

            PlayerReadyButton.gameObject.SetActive(false);
        }
        
        else{

            // sets custom properties for each player "isPlayerReady
            ExitGames.Client.Photon.Hashtable initializeProperties = new ExitGames.Client.Photon.Hashtable() { {Constants.PLAYER_READY, isPlayerReady} }; // ask sir for the explanation of the curly brackets
            PhotonNetwork.LocalPlayer.SetCustomProperties(initializeProperties);

            PlayerReadyButton.onClick.AddListener(() =>{

                isPlayerReady = !isPlayerReady;
                SetPlayerReady(isPlayerReady);

                ExitGames.Client.Photon.Hashtable newProperites = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady}};

                PhotonNetwork.LocalPlayer.SetCustomProperties(newProperites);
            });
        }
    }

    public void SetPlayerReady(bool playerReady){

        playerReadyImage.enabled = playerReady;

        if (playerReady){

            PlayerReadyButton.GetComponentInChildren<Text>().text = "Ready!";
        }

        else {

            PlayerReadyButton.GetComponentInChildren<Text>().text = "Ready?";
        }
    }
}
