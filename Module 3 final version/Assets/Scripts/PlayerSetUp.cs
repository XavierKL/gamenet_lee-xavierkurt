using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetUp : MonoBehaviourPunCallbacks
{

    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {   
        this.camera = transform.Find("Camera").GetComponent<Camera>();

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc")){

            GetComponent<VehicleMovment>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr")){

            GetComponent<VehicleMovment>().enabled = photonView.IsMine;
            //GetComponent<TakeDamage>().enabled = photonView.IsMine;
            //GetComponent<LapController>().enabled = photonView.IsMine; 
            //GetComponent<GunScripts>().enabled = photonView.IsMine;
            GetComponent<EliminationController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
