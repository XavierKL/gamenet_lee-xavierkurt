using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ProjectileScript : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyProjectile());
    }

    IEnumerator DestroyProjectile(){

        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider col){

        if (col.gameObject.tag == "Car" && col.GetComponent<PhotonView>().IsMine){

            col.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.All, 25);
        }
    }
}
