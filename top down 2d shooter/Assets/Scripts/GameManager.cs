using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{   
    public GameObject[] characters;
    public Transform[] spawnPoints;
    int actorNumber;
    public GameObject[] text;
    public static GameManager instance = null;

    void Awake(){

        if (instance == null){

            instance = this;
        }

        else if (instance != null){

            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        
        if (PhotonNetwork.IsConnectedAndReady){

            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)){

                actorNumber = PhotonNetwork .LocalPlayer.ActorNumber;
                InitialSpawn(playerSelectionNumber);
            }  
        }

        foreach (GameObject go in text){

            go.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitialSpawn(object pSNumber){

        int randomPosition = Random.Range(0, spawnPoints.Length);
        PhotonNetwork.Instantiate(characters[(int)pSNumber].name, spawnPoints[randomPosition].position, Quaternion.identity);
    }

    public void Respawn(GameObject player){

        int randomNumber = Random.Range(0, spawnPoints.Length);
        player.transform.position = spawnPoints[randomNumber].transform.position;
    }
}
