using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerSetUp : MonoBehaviourPunCallbacks
{

    public Camera camera;
    public Image image;
    // Start is called before the first frame update
    void Start()
    {   
        this.camera = transform.Find("Camera").GetComponent<Camera>();

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dm")){
            

            GetComponent<Movement>().enabled = photonView.IsMine;
            GetComponent<EliminationController>().enabled = photonView.IsMine;
            //GetComponent<Shooting>().enabled = photonView.IsMine;
            //spawn player UI prefab
            // this.image = playeruigo.find("healthbar").getcomponent<Image>();
            camera.enabled = photonView.IsMine;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
