using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float speed = 20;
    float dashDistance = 500f;
    float dashDuration = .05f;
    bool hasDashed = false;

    Vector3 previousDirection;
    Vector2 mousePosition;

    public Camera camera;

    Rigidbody rb;
        // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.LeftShift) && !hasDashed){

            StartCoroutine(Dashing());
            StartCoroutine(Cooldown());
        }

        #region Mouse Aiming
        Ray cameraRay = camera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, transform.position);
        float hitDist = 0.0f;


        if(groundPlane.Raycast(cameraRay, out hitDist)){

            Vector3 targetPoint = cameraRay.GetPoint(hitDist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            targetRotation.x = 0;
            targetRotation.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime);
        }
        #endregion

        if (this.transform.position.y <= 600){

            GameManager.instance.Respawn(this.gameObject);
        }
    }

    void FixedUpdate(){

        Vector3 dir = Vector3.zero;

        if (Input.GetKey(KeyCode.W)){

            dir += transform.forward;
        }

        if (Input.GetKey(KeyCode.A)){

            dir += -transform.right;
        }

        if (Input.GetKey(KeyCode.S)){

            dir += -transform.forward;
        }

        if (Input.GetKey(KeyCode.D)){

            dir += transform.right;
        }

        previousDirection = dir;

        rb.AddForce(dir * speed);

    }


    public IEnumerator Dashing(){

        rb.velocity = previousDirection.normalized * dashDistance;
        yield return new WaitForSeconds(dashDuration);
        rb.velocity = Vector3.zero;
        hasDashed = true;
    }

    public IEnumerator Cooldown(){

        yield return new WaitForSeconds(1.5f);
        hasDashed = false;
    }
}
