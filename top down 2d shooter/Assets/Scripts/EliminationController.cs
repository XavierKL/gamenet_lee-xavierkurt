
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class EliminationController : MonoBehaviourPunCallbacks
{
    private int eliminationOrderNumber = 0;
    
    public enum RaiseEventsCode{

        EliminationEventCode = 0
    }

    private void OnEnable(){

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable(){

        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent){

        if (photonEvent.Code == (byte)RaiseEventsCode.EliminationEventCode){

            object[] data = (object[]) photonEvent.CustomData;

            string nickNameOfEliminatedPlayer = (string)data[0];
            eliminationOrderNumber = (int)data[1];
            int viewID = (int)data[2];

            GameObject killedUIText = GameManager.instance.text[eliminationOrderNumber - 1];
            killedUIText.SetActive(true);

            if (viewID == photonView.ViewID){

                killedUIText.GetComponent<Text>().text = nickNameOfEliminatedPlayer + " " + " wins";
                killedUIText.GetComponent<Text>().color = Color.red;
            }

            else{

                killedUIText.GetComponent<Text>().text = eliminationOrderNumber + " " + nickNameOfEliminatedPlayer;
            }
        }
    }    

    public void Eliminated(){

        GetComponent<PlayerSetUp>().camera.transform.parent = null;
        GetComponent<Movement>().enabled = false;
        GetComponent<Shooting>().enabled = false;

        eliminationOrderNumber++;
        
        string nickName = photonView.Owner.NickName;
        int viewID = photonView.ViewID;

        object[] data = new object[] {nickName, eliminationOrderNumber, viewID};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions{

            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions{

            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.EliminationEventCode, data, raiseEventOptions, sendOptions);
    }

}
