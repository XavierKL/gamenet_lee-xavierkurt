using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public enum Character{

    DaInfant,
    TheNegotiator,
    ThePsycho
}

public class Shooting : MonoBehaviourPunCallbacks
{

    public Transform firepoint;

    [Header ("Hp items")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;
    public int playerDamage;
    
    public Character character;
    //public Text killText;

    public int score = 0;
    public Text scoreNumber;

    [Header("gun related things")]
    float timeInBetweenShots = 0f;
    public float fireRate;

    // Start is called before the first frame update
    void Start()
    {
        
        health = startHealth;
        healthBar.fillAmount = health / startHealth;

        switch(character){

            case Character.DaInfant:
            playerDamage = 50;
            fireRate = 5f;
            break;

            case Character.TheNegotiator:
            playerDamage = 30;
            fireRate = 15f;
            break;

            case Character.ThePsycho:
            playerDamage = 15;
            fireRate = 25f;
            break;

            default:
            break;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(0) && Time.time >= timeInBetweenShots){

            timeInBetweenShots = Time.time + 1f/fireRate;
            Shoot();
        }

        scoreNumber.text = score.ToString();

        if (score == 5 && photonView.IsMine){

            GetComponent<EliminationController>().Eliminated();
        }
    }

    public void Shoot(){

        RaycastHit hit;

        Ray ray = new Ray(this.firepoint.transform.position, this.firepoint.forward);

        if (Physics.Raycast(ray, out hit, 200)){

            Debug.Log(hit.collider.gameObject.name);

            Debug.DrawRay(firepoint.position, Vector3.forward, Color.red);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine){

                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, playerDamage);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info){

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0){

            Die(info);
            Debug.Log(info.Sender.NickName + " has Killed " + info.photonView.Owner.NickName);
        }
    }

    public void Die(PhotonMessageInfo info){

        if (photonView.IsMine){

            StartCoroutine(RespawnCountDown());
        }
        // change nickname to actornumber 
        else if (!photonView.IsMine && info.Sender.NickName == info.Sender.NickName){

            score++;
        }
    }

    IEnumerator RespawnCountDown(){

        float respawnTime = 5f;

        while (respawnTime > 0){

            yield return new WaitForSeconds(1f);
            respawnTime--;

            transform.GetComponent<Movement>().enabled = false;
        }

        GameManager.instance.Respawn(this.gameObject);
        transform.GetComponent<Movement>().enabled = true;
        photonView.RPC("RefilHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RefilHealth(){

        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
