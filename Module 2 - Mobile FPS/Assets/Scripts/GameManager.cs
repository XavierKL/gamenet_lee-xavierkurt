﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{

    public GameObject playerPrefab;
    public Transform[] respawnPoints;

    public static GameManager instance;

    private void Awake(){

        if (instance != null){

            Destroy(this.gameObject);
        }

        else {

            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        if (PhotonNetwork.IsConnectedAndReady){

            /*int randomPointX = Random.Range(-10, 10);
            int randomPointZ = Random.Range(-10, 10);
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(randomPointX, 0, randomPointZ), Quaternion.identity); */

            InitialSpawn();
        }
    }

    public void InitialSpawn(){

        int randomPosition = Random.Range(0, respawnPoints.Length);
        PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(respawnPoints[randomPosition].position.x, 0, respawnPoints[randomPosition].position.z), Quaternion.identity);
    }

    public void Respawn(GameObject player){

        int randomNumber = Random.Range(0, respawnPoints.Length);
        player.transform.position = respawnPoints[randomNumber].transform.position;
    }
}
