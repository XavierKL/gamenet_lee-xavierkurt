﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;


public class ShootingScript : MonoBehaviourPunCallbacks
{

    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header ("HP Things")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private Animator animator;
    private GameObject killText;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        killText = GameObject.Find("KilledPlayerText");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot(){

        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200)){

            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine){

                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info){

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0){

            Die(info);
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position){

        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die(PhotonMessageInfo info){

        if (photonView.IsMine){

            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountDown());
        }

        killText.GetComponent<Text>().text = info.Sender.NickName + " has killed " + info.photonView.Owner.NickName;
        StartCoroutine(KillTextLifeSpan());
    }

    IEnumerator RespawnCountDown(){

        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;

        while (respawnTime > 0){

            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are dead. Respawning in: " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        GameManager.instance.Respawn(this.gameObject);

        transform.GetComponent<PlayerMovementController>().enabled = true;
        photonView.RPC("HealthRefil", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void HealthRefil(){

        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    IEnumerator KillTextLifeSpan(){

        while (true){

            yield return new WaitForSeconds(2.0f);

            killText.GetComponent<Text>().text = "";
            
            yield return new WaitForSeconds(1.0f);
        }
    }
}
