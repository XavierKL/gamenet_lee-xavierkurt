﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PlayerSetUp : MonoBehaviourPunCallbacks
{
    
    public GameObject fpsModel;
    public GameObject nonFpsModel;

    public GameObject playerUIPrefab;

    public PlayerMovementController playerMovementController;
    public Camera fpsCamera;

    private Animator animator;
    public Avatar fpsAvator, nonFPSAvatar;

    private ShootingScript shoot;

    [Header("PlayerName")]
    public Text nameText;

    // Start is called before the first frame update
    void Start()
    {
        nameText.text = photonView.Owner.NickName;
        playerMovementController = this.GetComponent<PlayerMovementController>();
        animator = this.GetComponent<Animator>(); 

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);

        animator.SetBool("isLocalPlayer", photonView.IsMine);
        animator.avatar = photonView.IsMine ? fpsAvator : nonFPSAvatar;

        shoot = this.GetComponent<ShootingScript>();

        if (photonView.IsMine){

            GameObject playerUI = Instantiate(playerUIPrefab);
            playerMovementController.fixedTouchField = playerUI.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerUI.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            fpsCamera.enabled = true;

            playerUI.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shoot.Shoot());
        }

        else {

            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
